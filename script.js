let main = document.querySelector('main');
let prev = document.querySelector('.prev');
let next = document.querySelector('.next');
let image = document.querySelector('.image');
let title = document.querySelector('.title');
let counter = document.querySelector('.counter');
let mode = document.querySelector('.mode');
let cursor = document.querySelector('.cursor');
let splash = document.querySelector('.splash');

var darkMode = false;

mode.addEventListener('click', () => {
    console.log('click!')

    darkMode = !darkMode;

    if(darkMode == true){
        document.documentElement.style.setProperty('--main-color', '#fefefe');
        document.documentElement.style.setProperty('--sec-color', '#161616');
        document.documentElement.style.setProperty('--third-color', '#838383');
        document.documentElement.style.setProperty('--fourth-color', '#ded9d9');
    }else{
        document.documentElement.style.setProperty('--main-color', '#161616');
        document.documentElement.style.setProperty('--sec-color', '#fefefe');
        document.documentElement.style.setProperty('--third-color', '#ded9d9');
        document.documentElement.style.setProperty('--fourth-color', '#838383');
    }
});

window.addEventListener('mousemove', (e) => {
    cursor.style.transform = `translate3d(${e.clientX}px, ${e.clientY}px, 0)`;
});

next.addEventListener('mouseenter', () => {
    cursor.classList.remove('left');
    cursor.classList.add('right');
});

next.addEventListener('mouseleave', () => { 
    removeCursorClass(cursor);
})

prev.addEventListener('mouseenter', () => {
    cursor.classList.remove('right');
    cursor.classList.add('left');
});

prev.addEventListener('mouseleave', () => { 
    removeCursorClass(cursor);
});

function removeCursorClass(element){
    element.classList.remove('left');
    element.classList.remove('right');
}

let locations = ['AMSTERDAM', 'COPENHAGEN', 'HELSINKI', 'LONDON', 'NEW YORK', 'ROME']
image.style.backgroundImage = 'url(./media/1.jpg)';
counter.querySelector('p').innerText = '1/5';
title.querySelector('p').innerText = locations[0];

prev.addEventListener('click', () => {
    let num = +image.style.backgroundImage.split('/')[2][0];
    num == 1 ? num = 5 : num--;
    image.style.backgroundImage = `url(./media/${num}.jpg)`;
    counter.querySelector('p').innerText = `${num}/5`;
    title.querySelector('p').innerText = `${locations[num - 1]}`;
});

next.addEventListener('click', () => {
    let num = +image.style.backgroundImage.split('/')[2][0];
    num == 5 ? num = 1 : num++;
    image.style.backgroundImage = `url(./media/${num}.jpg)`;
    counter.querySelector('p').innerText = `${num}/5`;
    title.querySelector('p').innerText = `${locations[num - 1]}`;
});

function loadPage(){
    let html = '';
    let splashTitle = document.querySelector('.splash-title');
    let splashTitleArray = splashTitle.innerText.split('');
    splashTitleArray.forEach(letter => {
        html += `<span>${letter}</span>`
    })
    splashTitle.innerHTML = html;

    [...splashTitle.querySelectorAll('span')].forEach((span, idx) => {
        setTimeout(() => {
            span.style.color = 'var(--fourth-color)'
        }, (idx + 1) * 50);
    })

    setTimeout(() => {
        splash.classList.add('active');

        setTimeout(() => {
            main.style.transform = `translateY(0px)`;
            main.style.opacity = 1;
            splash.style.display = 'none';
            image.style.transform = 'scale(1)';
        }, 50)
    }, 2000)
};

loadPage();